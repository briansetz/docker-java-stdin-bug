import com.github.dockerjava.api.async.ResultCallback
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientConfig
import com.github.dockerjava.core.DockerClientImpl
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient
import java.io.*
import java.time.Duration


fun main() {
    val dockerConfig: DockerClientConfig = DefaultDockerClientConfig.createDefaultConfigBuilder().build()
    val dockerHttpClient = ApacheDockerHttpClient.Builder()
        .dockerHost(dockerConfig.dockerHost)
        .sslConfig(dockerConfig.sslConfig)
        .maxConnections(100)
        .connectionTimeout(Duration.ofSeconds(30))
        .responseTimeout(Duration.ofSeconds(45))
        .build()

    val dockerClient = DockerClientImpl.getInstance(dockerConfig, dockerHttpClient)

    dockerClient.pingCmd().exec()

    val imageId = "test:latest"
    val container = dockerClient.createContainerCmd(imageId)
        .withStdinOpen(true)
        .withStdInOnce(true)
        .exec()

    dockerClient.startContainerCmd(container.id).exec()

    // Better to use `using` from Kotlin's standard library to close the streams... but this is just a demo
    val output = PipedOutputStream()
    val input = PipedInputStream(output)
    val writer = BufferedWriter(OutputStreamWriter(output))
    val reader = BufferedReader(InputStreamReader(System.`in`))

    val resultCallback = dockerClient.attachContainerCmd(container.id)
        .withStdOut(true)
        .withStdErr(true)
        .withStdIn(input)
        .withFollowStream(true)
        .exec(ResultCallback.Adapter())

    // Removing this call (and all sleeps/wait for inputs) will cause the container to hang and never exit
    resultCallback.awaitStarted()

    writer.write("Hello World!\n")
    writer.flush()

    // Uncomment this to write output to the container yourself
//    val reader = BufferedReader(InputStreamReader(System.`in`))
//    while (true) {
//        val line = reader.readLine()
//        if (line == "!q") {
//            break
//        }
//        writer.write(line)
//        writer.write("\n")
//        writer.flush()
//    }

    println("Press any key to close streams")
    reader.readLine()

    println("Closing streams")
    reader.close()
    writer.close()
    input.close()
    output.close()
    println("Streams closed")

    // Sleep for 60 seconds before exiting, so you can see the container (not) exit
    println("Sleeping for 60 seconds before exiting")
    Thread.sleep(60*1000)
}